require 'rails_helper'
require "dinde/class"


RSpec.describe Class, :type => :model do
  before do
    load_all_classes
  end
  
  describe '.attr_type' do
    it 'return the attribute type as String' do
      object = ActiveRecord::Base.subclasses[1]
      attibute = object.attribute_names.first
      expect(object.attr_type(attibute)).to eq("integer")
    end
  end

  describe '.my_public_methods' do
    it 'only return the public methods from a class in correct location' do
    ## Only files on Dir["#{Rails.root}/app/models/*.rb"] 
      expect(Car.my_public_methods).to eq([:start])
    end
  end

  describe '.ignored_attrs' do
    it 'must to have an ingnored_attributes list' do
      expect(Car.ignored_attrs).to include("created_at", "updated_at")
    end
  end
  
  describe '.class_attrs' do
    it 'must to ignore the Objects attrs' do
      class_exemple = Dinde.app_models.first
      expect(class_exemple.class_attrs).to_not include(Class.ignored_attrs)
    end
  end

end

def load_all_classes
  Dir[Rails.root + "app/models/**/*.rb"].each do |path|
    require path
  end
end
