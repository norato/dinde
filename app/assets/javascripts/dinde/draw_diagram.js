$(document).ready(function(){
  for (index in gon.app_models) {
    model = gon.app_models[index];
    name = model.name;
    attrs = model.attributes;
    methods = model.methods;
    diagram = new ClassDiagram(name, attrs, methods);
    diagram.drawClass();
  };
});