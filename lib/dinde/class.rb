class Class
  def attr_type(_attr)
    self.columns_hash[_attr].type.to_s
  end

  def my_public_methods
    methods = Array.new
    models = Dir["#{Rails.root}/app/models/*.rb"]
    new.public_methods(false).map { |method|
      file = new.public_method(method).source_location.first
      if models.include? file
        methods.append method
      end
    }
    return methods
  end

  def ignored_attrs
    ["created_at", "updated_at"]
  end

  def class_attrs
    attribute_names.delete_if {|x| ignored_attrs.include?(x)}
  end
  # Car.reflect_on_all_associations.collect {|a| [a.macro, a.name]}
end