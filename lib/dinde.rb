require "dinde/engine"
require "dinde/class"

module Dinde

  Classe = Struct.new(:name, :attributes, :methods)

  def self.extract_classes
    classes = Array.new
    app_models.each do |app_model|
        methods = Array.new
        attrs = Array.new
        app_model.class_attrs.each do |model_attr|
            attr_type =  app_model.attr_type(model_attr) 
            attrs.append(["public", model_attr, attr_type])
        end
        app_model.my_public_methods.each do |method|
          methods.append(["public", method])
        end

        app_model.new.private_methods(false).each do |method|
          methods.append(["private", method])
        end
        _class = Classe.new(app_model.name, attrs, methods)
        classes.append(_class)
      end
    return classes
  end



  private
  def self.load_all_models
    Dir[Rails.root + "app/models/**/*.rb"].each do |path|
      require path
    end  
  end

  def self.app_models
    load_all_models
    ActiveRecord::Base.subclasses.delete_if {|x| ignored_classes.include?(x)}
  end

  def self.ignored_classes
    [ActiveRecord::SchemaMigration]
  end
end
