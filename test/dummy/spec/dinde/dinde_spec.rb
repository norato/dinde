require 'rails_helper'

RSpec.describe Dinde, :type => :model do

  describe '.load_all_models' do
    files = Dir["#{Rails.root}/app/models/*.rb"]
    it 'only load files on app/models' do
      expect(Dinde.load_all_models).to match_array(files)
    end
    it 'must to have the same length of files' do
      expect(Dinde.load_all_models.length).to be(files.length)
    end  
  end

  describe '.ignored_classes' do
    it 'must to have an ignored_classes list' do
      expect(Dinde.ignored_classes).to include(ActiveRecord::SchemaMigration)
    end
  end    
  
  describe '.app_models' do
    it 'must to ignore the Rails classes' do
      length = Dir["#{Rails.root}/app/models/*.rb"].length
      expect(Dinde.app_models.length).to be(length) 
      expect(Dinde.app_models).to_not include(Dinde.ignored_classes)
    end
  end  


  describe '.extract_classes' do
    it 'return the length of classes' do
      length = Dir["#{Rails.root}/app/models/*.rb"].length
      expect(Dinde.extract_classes.length).to be(length)
    end

    it 'each class will returned by a list of structs' do
      expect(Dinde.extract_classes.class).to be_a_kind_of(Array.class)
      expect(Dinde.extract_classes.first.class).to be_a_kind_of(Struct.class)
    end

    describe "Structure of the Struct" do
      struct = Dinde.extract_classes.first
      it 'has a Name as String' do
        expect(struct.name.class).to be_a_kind_of(String.class)
      end
      it 'has a list of list of Attributes' do
        expect(struct.attributes.class).to be_a_kind_of(Array.class)
        expect(struct.attributes.first.class).to be_a_kind_of(Array.class)
      end
      it 'has a list of list of Methods' do
        expect(struct.methods.class).to be_a_kind_of(Array.class)
        expect(struct.methods.first.class).to be_a_kind_of(Array.class)
      end
    end
  end
end
