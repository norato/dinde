require_dependency "dinde/application_controller"
require 'dinde'

module Dinde
  class DindeController < ApplicationController
    def index
    	gon.app_models = Dinde.extract_classes
    end
  end
end
