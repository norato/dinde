$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "dinde/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "dinde"
  s.version     = Dinde::VERSION
  s.description = "A Simple gem to extract the diagram class from a rails project"
  s.summary     = "DINDE - Dinde is not a Diagram Editor"
  s.author      = "Felipe Norato"
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.rdoc"]
  s.test_files = Dir["test/**/*"]

  s.add_dependency "rails", "~> 4.1.6"

  s.add_development_dependency "sqlite3"
  s.add_development_dependency "rspec-rails"
end
